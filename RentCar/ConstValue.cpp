// � ����� �������� ������ ���������
#pragma once
#include <string>

using namespace std;

const string NAME_FILE_RENT_CAR = "rentcar.txt";
const string NAME_FILE_CAR = "car.txt";
const string IS_NOT_OPEN_FILE = "���� �� ���������� �������!!!";
const string IS_FILE_EMPTY = "���� ������";
const string NAME_BODY_TYPE = "BodyType";
const string NAME_CAR_CLASSES = "CarClasses";
const string NAME_TRANSMISSION_TYPE = "TransmissionType";
const string NAME_CITY = "City";

const string TEXT_RED_COLOR = "\033[31m";
const string TEXT_WHITE_COLOR = "\033[0m";
const string TEXT_GREEN_COLOR = "\033[32m";

const size_t MAX_LENGTH_ID = 36;
const size_t INDEX_FOR_BACK = 999;
const size_t MAX_LENGTH_NAME = 10;

const char FILE_SEPARATOR = '#';