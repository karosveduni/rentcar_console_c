#pragma once

#include <vcruntime_exception.h>
#include <string>

class back_exception : public std::exception
{
private:
	std::string message;

public:

	back_exception(const std::string& message) : message(message) {}

	const char* what() const noexcept override {
		return message.c_str();
	}

	back_exception() = default;
};

