#pragma once

#include <string>
#include <random>

using namespace std;

static class RandomString
{
private:
	RandomString() = default;
	bool operator==(const RandomString& other) const = default;

public:
	static string get_random_string(const size_t& length);
};

