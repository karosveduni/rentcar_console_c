﻿#include <iostream>
#include <string>
#include <tuple>
#include <functional>
#include <vector>
#include <fstream>
#include <filesystem>
#include <Windows.h>
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <sstream>
#include <map>
#include <algorithm>
#include <regex>

#include "Car.h"
#include "Client.h"
#include "back_exception.h"

using namespace std;

void view_car();
void back();
void view_client();
void rent_car();
void edit_about_car();
void settings_app();
void exit_program();

vector<string> read_all_items_from_file(string name_file);
void write_all_items_in_file(const string& name_file, const vector<string>& vector);
void write_one_item_in_file(string name_file, string item);
void is_file_exists_or_create(string name_file);

void print_table_car();
void print_table_client();

void print_menu(string title_menu, vector<tuple<int, string, function<void()>>> menu);

void add_new_car();
void input_car(const string& title, function<void(const string&)> func);
int input_enum(string title, vector<string> vector);
void filter_table_car();

string body_type_conver_to_string(BodyType body_type);
string car_classes_convert_to_string(CarClasses car_classes);
string transmission_type_convert_to_string(TransmissionType transmission_type);
string city_convert_to_string(City city);

void clear_file();
void input_answer(function<void()> func);
void init_answers();
void open_file_for_clear_file(string name_file);
void delete_file();

void remove_file_from_disk(string name_file);

bool case_insensitive_string_compare(const string& str1, const string& str2);
bool case_insensitive_compare(char c1, char c2);

vector<string> get_name_value_enum(string name_type_enum);

vector<string> car_vector_convert_to_string_vector(vector<Car> items);

string find_name_car(string id);

vector<Car> reserve_car_list;
vector<Car> cars;
vector<Client> reserve_client_list;
vector<Client> clients;

vector<string> answers;

int main()
{
	setlocale(LC_ALL, "Russian");
	HWND hwnd = GetConsoleWindow();
	SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_CAPTION);
	ShowWindow(hwnd, SW_SHOWMAXIMIZED);

	is_file_exists_or_create(NAME_FILE_CAR);
	is_file_exists_or_create(NAME_FILE_RENT_CAR);

	vector<string> vc = read_all_items_from_file(NAME_FILE_CAR);
	if (!vc.empty())
	{
		for (const auto& str : vc)
		{
			cars.push_back(Car::parse(str));
		}

	}
	vc = read_all_items_from_file(NAME_FILE_RENT_CAR);
	if (!vc.empty())
	{
		for (const auto& str : vc)
		{
			clients.push_back(Client::parse(str));
		}
	}

	vector<tuple<int, string, function<void()>>> menu_items;

	menu_items.push_back(make_tuple(1, "Просмотреть список автомобилей", &view_car));
	menu_items.push_back(make_tuple(2, "Просмотреть список клиентов", &view_client));
	menu_items.push_back(make_tuple(3, "Дать автомобиль в оренду", &rent_car));
	menu_items.push_back(make_tuple(4, "Редактировать информацию про автомобили", &edit_about_car));
	menu_items.push_back(make_tuple(5, "Настройки программы", &settings_app));
	menu_items.push_back(make_tuple(INDEX_FOR_BACK, "Выйти из программы", &exit_program));

	print_menu("Главное меню", menu_items);

	return 0;
}

void view_car()
{

	print_table_car();

	vector<tuple<int, string, function<void()>>> menu_item;

	menu_item.push_back(make_tuple(1, "Добавить новий автомобиль", &add_new_car));
	menu_item.push_back(make_tuple(2, "Фильтровать список", &filter_table_car));
	menu_item.push_back(make_tuple(3, "Обновить таблицу", &print_table_car));
	menu_item.push_back(make_tuple(INDEX_FOR_BACK, "Назад", &back));

	print_menu("Фильтры для поиска", menu_item);

}

void back()
{
	throw back_exception("");
}

void view_client()
{
	print_table_client();
}

void rent_car()
{
	print_table_car();

	string str_index;
	size_t index = 0;
	cout << "Выберите автомобиль введя его номер или " << INDEX_FOR_BACK << " (Назад) =>";
	getline(cin, str_index);

	istringstream iss(str_index);

	if (iss >> index && index <= cars.size())
	{
		index--;
		if (cars[index].get_is_rent())
		{
			cout << "Автомобиль уже в оренде." << endl;
			return;
		}

		string name, second_name, number_passport, id_car, card_number, day;
		regex passport_regex("\\d{9}");
		regex credit_card_regex("^[0-9]{13,19}$");

		cout << "Введите имя клиента => ";
		getline(cin, name);

		cout << "Введите фамилию клиента => ";
		getline(cin, second_name);

		do {
			cout << "Введите номер паспорта => ";
			getline(cin, number_passport);
		} while (!regex_match(number_passport, passport_regex));

		do {
			cout << "Введите номер карты => ";
			getline(cin, card_number);
		} while (!regex_match(card_number, credit_card_regex));

		int d;
		while (true) {
			cout << "Введите на сколько дней хотите взять => ";
			getline(cin, day);

			istringstream iss(day);

			if (iss >> d) break;

		}

		Client client(name, second_name, number_passport, cars[index].get_id(), card_number, d);

		clients.push_back(client);
		cars[index].set_is_rent(true);

		write_one_item_in_file(NAME_FILE_RENT_CAR, client.to_string());
		write_all_items_in_file(NAME_FILE_CAR, car_vector_convert_to_string_vector(cars));

		cout << TEXT_GREEN_COLOR << "Автомобиль успешно в оренде." << TEXT_WHITE_COLOR << endl;
	}
	else
	{
		if (index == INDEX_FOR_BACK) back();

		cout << "Автомобиля с таким номером нет." << endl;
	}
}

void edit_about_car()
{
	if (!cars.empty())
	{
		print_table_car();

		string str_index;
		size_t index = 0;
		while (true)
		{

			cout << "Выберите автомобиль введя его номер или " << INDEX_FOR_BACK << " (Назад) =>";
			getline(cin, str_index);

			istringstream iss(str_index);

			if (iss >> index && index <= cars.size())
			{
				index--;
				vector<tuple<int, string, function<void()>>> menu_item;
				menu_item.push_back(make_tuple(1, "Название автомобиля",
					[&index]()
					{
						input_car("Введите новое название => ",
						[&index](string s)
							{
								cars[index].set_name(s);
							});
					}));
				menu_item.push_back(make_tuple(2, "Рейтинг автомобиля",
					[&index]()
					{
						input_car("Введите новый рейтинг =>",
						[&index](string s)
							{
								cars[index].set_rating(std::byte(stoi(s)));
							});
					}));
				menu_item.push_back(make_tuple(3, "Потребление топлива",
					[&index]()
					{
						input_car("Введите расход топллива => ",
						[&index](string s) { cars[index].set_fuel_consumption(stof(s)); });
					}));
				menu_item.push_back(make_tuple(4, "Залог",
					[&index]()
					{
						input_car("Введите залог для автомобиля => ",
						[&index](string s) { cars[index].set_pledge(stoi(s)); });
					}));
				menu_item.push_back(make_tuple(5, "Цена оренды",
					[&index]()
					{
						input_car("Введите цену оренды => ",
						[&index](string s) { cars[index].set_rental_prices(stoi(s)); });
					}));
				menu_item.push_back(make_tuple(6, "Кузов",
					[&index]()
					{
						vector<string> vector = get_name_value_enum(NAME_BODY_TYPE);
						cars[index].set_body_type((BodyType)input_enum("Выберете кузов", vector));
					}));
				menu_item.push_back(make_tuple(7, "Класс автомобиля",
					[&index]()
					{
						auto vector = get_name_value_enum(NAME_CAR_CLASSES);
						cars[index].set_car_classes((CarClasses)input_enum("Выберите класс автомобиля", vector));
					}));
				menu_item.push_back(make_tuple(8, "Коробка передач",
					[&index]()
					{
						auto vector = get_name_value_enum(NAME_TRANSMISSION_TYPE);
						cars[index].set_transmission_type((TransmissionType)input_enum("Коробка передач", vector));
					}));
				menu_item.push_back(make_tuple(9, "Город",
					[&index]()
					{
						auto vector = get_name_value_enum(NAME_CITY);
						cars[index].set_city((City)input_enum("Выберите город", vector));
					}));
				menu_item.push_back(make_tuple(10, "Обновить таблицу", &print_table_car));
				menu_item.push_back(make_tuple(INDEX_FOR_BACK, "Назад", &back));


				stringstream ss;
				ss << "Редактировать информацию про автомобиль " << cars[index].get_name();

				print_menu(ss.str(), menu_item);

				write_all_items_in_file(NAME_FILE_CAR, car_vector_convert_to_string_vector(cars));

				return;
			}

			if (index == INDEX_FOR_BACK) return;
		}
	}

	cout << "Атомобилей нет." << endl;

}

void settings_app()
{
	vector<tuple<int, string, function<void()>>> menu_item;

	menu_item.push_back(make_tuple(1, "Очистить файл", &clear_file));
	menu_item.push_back(make_tuple(2, "Удалить файл", &delete_file));
	menu_item.push_back(make_tuple(INDEX_FOR_BACK, "Назад", &back));

	print_menu("Настройки программы", menu_item);
}

void exit_program()
{
	exit(EXIT_SUCCESS);
}

vector<string> read_all_items_from_file(string name_file)
{

	ifstream read_file(name_file);
	if (!read_file.is_open())
	{
		cout << IS_NOT_OPEN_FILE << endl;
		exit_program();
	}

	read_file.seekg(0, ios::end);
	if (read_file.tellg() == 0)
	{
		return vector<string>();
	}

	read_file.seekg(0, ios::beg);
	vector<string> vector;
	string line;
	while (getline(read_file, line))
	{
		vector.push_back(line);
	}

	read_file.close();
	return vector;
}

void write_all_items_in_file(const string& name_file, const vector<string>& vector)
{
	ofstream write_file(name_file);
	if (!write_file.is_open())
	{
		cout << IS_NOT_OPEN_FILE << endl;
		exit_program();
	}

	for (const string& value : vector) {
		write_file << value << endl;
	}

	write_file.close();
}

void write_one_item_in_file(string name_file, string item)
{
	ofstream write_file(name_file, ios::app);
	if (!write_file.is_open())
	{
		cout << IS_NOT_OPEN_FILE << endl;
		exit_program();
	}

	write_file << item << endl;

	write_file.close();
}

vector<string> get_name_value_enum(string name_type_enum)
{
	vector<string> vector;

	if (name_type_enum == NAME_BODY_TYPE)
	{
		vector.push_back(body_type_conver_to_string(BodyType::Sedan));
		vector.push_back(body_type_conver_to_string(BodyType::StationWagon));

		return vector;
	}

	if (name_type_enum == NAME_CAR_CLASSES)
	{
		vector.push_back(car_classes_convert_to_string(CarClasses::Average));
		vector.push_back(car_classes_convert_to_string(CarClasses::Business));
		vector.push_back(car_classes_convert_to_string(CarClasses::Premium));
		vector.push_back(car_classes_convert_to_string(CarClasses::Steward));
		vector.push_back(car_classes_convert_to_string(CarClasses::SUV));

		return vector;
	}

	if (name_type_enum == NAME_TRANSMISSION_TYPE)
	{
		vector.push_back(transmission_type_convert_to_string(TransmissionType::Machine));
		vector.push_back(transmission_type_convert_to_string(TransmissionType::Mechanics));

		return vector;
	}

	if (name_type_enum == NAME_CITY)
	{
		vector.push_back(city_convert_to_string(City::Dnepr));
		vector.push_back(city_convert_to_string(City::Kyiv));
		vector.push_back(city_convert_to_string(City::Lviv));
		vector.push_back(city_convert_to_string(City::Odessa));

		return vector;
	}

	throw exception("Нету нужного значения.");

}

void update_item_in_file(const string& name_file)
{
	ifstream file(name_file);

	if (!file.is_open())
	{
		cerr << TEXT_RED_COLOR << IS_NOT_OPEN_FILE << TEXT_WHITE_COLOR << endl;
		return;
	}


}

void is_file_exists_or_create(string name_file)
{
	if (!filesystem::exists(name_file))
	{
		ofstream file(name_file);
		if (!file.is_open())
		{
			cout << "Файл не создан!!!" << endl << "Проверьте файл. Программа не способна продолжить работу. Создайте файл вручную с названием \"" << name_file << "\"" << endl;
			exit_program();
		}
		file.close();
	}
}

void print_table_car()
{
	if (cars.empty())
	{
		cout << "Файл пустой" << endl;
		return;
	}

	cout << setw(65) << "Список машин" << endl;

	cout << "+---+-------------------+-------+-------------------+-----+-----------+----------+----------------+---------------+-------+--------+" << endl;
	cout << '|' << setw(3) << '№' << '|' << "Название автомобиля" << '|' << "Рейтинг" << '|' << "Потребления топлива" << '|' << "Залог" << '|' << "Цена оренды" << '|' << setw(10) << left << "Кузов" << '|' << "Класс автомобиля" << '|' << "Коробка передач" << '|' << setw(7) << left << "Город" << '|' << "В оренде" << '|' << endl;
	cout << "+---+-------------------+-------+-------------------+-----+-----------+----------+----------------+---------------+-------+--------+" << endl;

	for (int i = 0; i < cars.size(); i++)
	{
		cout << '|' << setw(3) << internal << i + 1
			<< '|' << setw(19) << left << cars[i].get_name()
			<< '|' << setw(7) << left << static_cast<int>(cars[i].get_rating())
			<< '|' << setw(19) << left << cars[i].get_fuel_consumption()
			<< '|' << setw(5) << left << cars[i].get_pledge()
			<< '|' << setw(11) << left << cars[i].get_rental_prices()
			<< '|' << setw(10) << left << body_type_conver_to_string(cars[i].get_body_type())
			<< '|' << setw(16) << left << car_classes_convert_to_string(cars[i].get_car_classes())
			<< '|' << setw(15) << left << transmission_type_convert_to_string(cars[i].get_transmission_type())
			<< '|' << setw(7) << left << city_convert_to_string(cars[i].get_city())
			<< '|' << setw(8) << left << (cars[i].get_is_rent() ? "+" : "-")
			<< '|' << endl;
	}

	cout << "+---+-------------------+-------+-------------------+-----+-----------+----------+----------------+---------------+-------+--------+" << endl;
}

void print_table_client()
{
	if (clients.empty())
	{
		cout << TEXT_RED_COLOR << "Файл пустой." << TEXT_WHITE_COLOR << endl;
		return;
	}

	cout << setw(51) << "Список клиентов" << endl;

	cout << "+---+-----------+----------+--------------+--------------------+---------------------+-------------------+" << endl;
	cout << '|' << setw(3) << '№'
		<< '|' << "Имя клиента"
		<< '|' << setw(10) << left << "Фамилия"
		<< '|' << "Номер паспорта"
		<< '|' << setw(20) << left << "Название автомобиля"
		<< '|' << "Дата окончания оренды"
		<< '|' << setw(19) << left << "Банковская карта" << '|'
		<< endl;
	cout << "+---+-----------+----------+--------------+--------------------+---------------------+-------------------+" << endl;

	for (int i = 0; i < clients.size(); i++)
	{
		cout << '|' << setw(3) << i + 1
			<< '|' << setw(11) << left << clients[i].get_name()
			<< '|' << setw(10) << left << clients[i].get_second_name()
			<< '|' << setw(14) << left << clients[i].get_number_passport()
			<< '|' << setw(20) << left << find_name_car(clients[i].get_id_car())
			<< '|' << setw(21) << left << clients[i].get_date()
			<< '|' << setw(19) << left << clients[i].get_card_number()
			<< '|' << endl;
	}

	cout << "+---+-----------+----------+--------------+--------------------+---------------------+-------------------+" << endl;
}

void print_menu(string title_menu, vector<tuple<int, string, function<void()>>> menu)
{
	string value = "";
	int value_int = 0;

	while (true)
	{
		cout << "\t" << title_menu << endl;
		for (const auto& item : menu)
		{
			cout << get<0>(item) << ") " << get<1>(item) << endl;
		}

		cout << "Выберите пунк меню => ";
		getline(cin, value);

		if (!value.empty())
			try
		{
			value_int = stoi(value);
			vector<tuple<int, string, function<void()>>>::iterator it = find_if(menu.begin(), menu.end(),
				[value_int](tuple<int, string, function<void()>> tuple)
				{
					return get<0>(tuple) == value_int;
				});

			if (it == menu.end())
			{
				cout << TEXT_RED_COLOR << "Даного пункта нет." << TEXT_WHITE_COLOR << endl;
			}
			else
			{
				get<2>(*it)();
			}

		}
		catch (const invalid_argument& ex)
		{
			cout << TEXT_RED_COLOR << "Вы ввели не число. Введите пожайлуста число." << TEXT_WHITE_COLOR << endl;
			cerr << ex.what() << endl;
		}
		catch (const out_of_range& ex)
		{
			cout << TEXT_RED_COLOR << "Значение превысело допустимый максимум. Max => " << INT32_MAX << TEXT_WHITE_COLOR << endl;
			cerr << ex.what() << endl;
		}
		catch (const back_exception&)
		{
			system("cls");
			return;
		}
		catch (const exception& ex)
		{
			cout << TEXT_RED_COLOR << "Была поймана ошибка # " << ex.what() << TEXT_WHITE_COLOR << endl;
		}
	}
}

void add_new_car()
{
	Car c;

	cout << "Добавление нового автомобиля" << endl;

	input_car("Введите название нового автомобиля => ",
		[&c](string s) { c.set_name(s); });

	input_car("Введите рейтинг нового автомобиля => ",
		[&c](string s) { c.set_rating(std::byte(stoi(s))); });

	input_car("Введите расход топллива => ",
		[&c](string s) { c.set_fuel_consumption(stof(s)); });

	input_car("Введите залог для автомобиля => ",
		[&c](string s) { c.set_pledge(stoi(s)); });

	input_car("Введите цену оренды => ",
		[&c](string s) { c.set_rental_prices(stoi(s)); });

	vector<string> vector = get_name_value_enum(NAME_BODY_TYPE);
	c.set_body_type((BodyType)input_enum("Выберете кузов", vector));

	vector = get_name_value_enum(NAME_CAR_CLASSES);
	c.set_car_classes((CarClasses)input_enum("Выберите класс автомобиля", vector));

	vector = get_name_value_enum(NAME_TRANSMISSION_TYPE);
	c.set_transmission_type((TransmissionType)input_enum("Коробка передач", vector));

	vector = get_name_value_enum(NAME_CITY);
	c.set_city((City)input_enum("Выберите город", vector));

	write_one_item_in_file(NAME_FILE_CAR, c.to_string());
	cars.push_back(c);

}

void input_car(const string& title, function<void(const string&)> func)
{
	string fieldValue;
	while (true)
	{
		try {
			cout << title;
			getline(cin, fieldValue);
			func(fieldValue);
			return;
		}
		catch (exception ex) {
			cout << TEXT_RED_COLOR << ex.what() << TEXT_WHITE_COLOR << endl;
		}
	}
}

int input_enum(string title, vector<string> vector)
{
	cout << "\t" << title << endl;

	for (int i = 0; i < vector.size(); i++)
	{
		cout << i + 1 << ") " << vector[i] << endl;
	}

	string value;
	int result = 0;
	size_t size = vector.size();

	while (true)
	{
		cout << "Выберите пункт => ";
		getline(cin, value);

		try {
			result = stoi(value);
			if (result > 0 && result <= size)
			{
				return result - 1;
			}
			else
			{
				cout << TEXT_RED_COLOR << "Значение должно быть от 1 до " << vector.size() << TEXT_WHITE_COLOR << endl;
			}
		}
		catch (const invalid_argument&)
		{
			cout << TEXT_RED_COLOR << "Строка не нужного формата." << TEXT_WHITE_COLOR << endl;
		}
		catch (const out_of_range&)
		{
			cout << TEXT_RED_COLOR << "Значение было больше или меньше допустимого." << endl;
		}
	}
}

vector<tuple<int, string, function<void()>>> columns;

void init_columns()
{
	if (columns.empty())
	{
		columns.push_back(make_tuple(1, "Название", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_name() < c2.get_name(); }); }));
		columns.push_back(make_tuple(2, "Рейтинг", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_rating() < c2.get_rating(); }); }));
		columns.push_back(make_tuple(3, "Потребление топлива", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_fuel_consumption() < c2.get_fuel_consumption(); }); }));
		columns.push_back(make_tuple(4, "Залог", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_pledge() < c2.get_pledge(); }); }));
		columns.push_back(make_tuple(5, "Цена оренды", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_rental_prices() < c2.get_rental_prices(); }); }));
		columns.push_back(make_tuple(6, "Кузов", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_body_type() < c2.get_body_type(); }); }));
		columns.push_back(make_tuple(7, "Класс автомобиля", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_car_classes() < c2.get_car_classes(); }); }));
		columns.push_back(make_tuple(8, "Коробка передач", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_transmission_type() < c2.get_transmission_type(); }); }));
		columns.push_back(make_tuple(9, "Город", []() { sort(cars.begin(), cars.end(), [](Car& c1, Car& c2) -> bool { return c1.get_city() < c2.get_city(); }); }));
		columns.push_back(make_tuple(0, "Обновить таблицу", &print_table_car));
		columns.push_back(make_tuple(INDEX_FOR_BACK, "Назад", &back));
	}
}

void filter_table_car()
{
	init_columns();

	print_menu("Сортировать список автомобиля", columns);
}

string body_type_conver_to_string(BodyType body_type)
{
	switch (body_type)
	{
	case Sedan: return "Седан";
	case StationWagon: return "Универсал";
	}

	throw exception("Значение не верное.");
}

string car_classes_convert_to_string(CarClasses car_classes)
{
	switch (car_classes)
	{
	case Steward: return "Оптимальный";
	case Average: return "Средний";
	case Business: return "Бизнес";
	case SUV: return "Внедорожник";
	case Premium: return "Премиум";
	}

	throw exception("Значение не верное.");
}

string transmission_type_convert_to_string(TransmissionType transmission_type)
{
	switch (transmission_type)
	{
	case Mechanics: return "Механика";
	case Machine: return "Автомат";
	}

	throw exception("Значение не верное.");
}

string city_convert_to_string(City city)
{
	switch (city)
	{
	case Dnepr: return "Днепр";
	case Lviv: return "Львов";
	case Kyiv: return "Киев";
	case Odessa: return "Одесса";
	}

	throw exception("Значение не верное.");
}

void clear_file()
{
	init_answers();

	input_answer([]()
		{
			open_file_for_clear_file(NAME_FILE_CAR);
			open_file_for_clear_file(NAME_FILE_RENT_CAR);
		});
}

void input_answer(function<void()> func)
{
	string answer;
	cout << "Вы уверены что хотите это сделать ?"
		<< "\nY - yes, N - No, T - true, F - false, Д - да, н - нет"
		<< "\nВведите ваш ответ => " << endl;
	getline(cin, answer);

	for (int i = 0; i < answers.size(); i++)
	{
		if (case_insensitive_string_compare(answers[i], answer))
		{
			if (i > -1 && i < 6)
			{
				func();
				return;
			}
		}
	}
}

void init_answers()
{
	if (answers.empty())
	{
		answers.push_back("y");
		answers.push_back("yes");
		answers.push_back("д");
		answers.push_back("да");
		answers.push_back("t");
		answers.push_back("true");

		answers.push_back("n");
		answers.push_back("no");
		answers.push_back("f");
		answers.push_back("false");
		answers.push_back("н");
		answers.push_back("нет");
	}
}

void open_file_for_clear_file(string name_file)
{
	ofstream file(name_file, ios::trunc);

	if (!file.is_open())
	{
		cerr << TEXT_RED_COLOR << "Файла возможно не существует! Имя файла =>" << name_file << TEXT_WHITE_COLOR << endl;
	}
	else
	{
		cout << "Файл был успешно очищен. Имя файла" << name_file << endl;
	}

	file.close();
}

void delete_file()
{
	init_answers();

	input_answer([]()
		{
			remove_file_from_disk(NAME_FILE_CAR);
			remove_file_from_disk(NAME_FILE_RENT_CAR);
		});
}

void remove_file_from_disk(string name_file)
{
	if (remove(name_file.c_str()) == 0)
	{
		cout << "Файл успешно удален. Имя файла => " << name_file << endl;
	}
	else
	{
		cout << "Файл не удалось удалить файл. Имя файла => " << name_file << endl;
	}
}

bool case_insensitive_string_compare(const string& str1, const string& str2) {
	return str1.size() == str2.size() &&
		std::equal(str1.begin(), str1.end(), str2.begin(), case_insensitive_compare);
}

bool case_insensitive_compare(char c1, char c2) {
	return std::tolower(static_cast<unsigned char>(c1)) == std::tolower(static_cast<unsigned char>(c2));
}

vector<string> car_vector_convert_to_string_vector(vector<Car> items)
{
	if (items.empty())
	{
		return vector<string>();
	}

	vector<string> v;

	for (Car& c : items)
	{
		v.push_back(c.to_string());
	}

	return v;
}

string find_name_car(string id)
{
	if (cars.empty() || id.empty())
	{
		return string();
	}

	for (Car& c : cars)
	{
		if (c.get_id() == id)
		{
			return c.get_name();
		}
	}

	return string();
}
