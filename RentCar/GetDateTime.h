#pragma once

#include <chrono>

using namespace std;

class GetDateTime
{
private:
    int year;
    int month;
    int day;

    GetDateTime() : year(0), month(0), day(0) {}
    ~GetDateTime() {}

    GetDateTime(const GetDateTime& dt) = delete;
    GetDateTime& operator=(const GetDateTime dt) = delete;

public:
    void get_date_time_now(int add_day = 0);

    int get_year();
    int get_month();
    int get_day();


    static GetDateTime& get_instance()
    {
        static GetDateTime dt;
        return dt;
    }
};

