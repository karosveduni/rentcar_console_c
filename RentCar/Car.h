#pragma once
#include <string>
#include <sstream>

#include "Enum.cpp"
#include "RandomString.h"
#include "ConstValue.cpp"

using namespace std;

class Car
{
private:
	string id;
	//�� ������ 10 ��������
	string name;	
	std::byte rating;
	float fuel_consumption;
	int pledge;
	int rental_prices;
	BodyType body_type;
	CarClasses car_classes;
	TransmissionType transmission_type;
	City city;
	bool is_rent;

public:

	Car() : is_rent(false)
	{
		id = RandomString::get_random_string(MAX_LENGTH_ID);
	}

	string get_id();

	void set_name(string new_name);
	string get_name();

	void set_rating(std::byte new_rating);
	std::byte get_rating();

	void set_fuel_consumption(float new_fuel_consumption);
	float get_fuel_consumption();

	void set_pledge(int new_pladge);
	int get_pledge();

	void set_rental_prices(int new_rental_prices);
	int get_rental_prices();

	void set_body_type(BodyType new_body_type);
	BodyType get_body_type();

	void set_car_classes(CarClasses new_car_classes);
	CarClasses get_car_classes();

	void set_transmission_type(TransmissionType new_transmission_type);
	TransmissionType get_transmission_type();

	void set_city(City new_city);
	City get_city();

	void set_is_rent(bool new_is_rent);
	bool get_is_rent();

	bool operator==(const Car& other) const {
		return id == other.id && name == other.name && rating == other.rating &&
			fuel_consumption == other.fuel_consumption && pledge == other.pledge &&
			rental_prices == other.rental_prices && body_type == other.body_type &&
			car_classes == other.car_classes && transmission_type == other.transmission_type &&
			city == other.city && is_rent == other.is_rent;
	}

	bool operator!=(const Car& other) const {
		return !(*this == other);
	}

	Car(const string& name,
		const std::byte& rating,
		float fuel_consumption,
		int pledge,
		int rental_prices,
		const BodyType& body_type,
		const CarClasses& car_classes,
		const TransmissionType& transmission_type,
		const City& city)
		: id(RandomString::get_random_string(MAX_LENGTH_ID)),
		name(name),
		rating(rating),
		fuel_consumption(fuel_consumption),
		pledge(pledge),
		rental_prices(rental_prices),
		body_type(body_type),
		car_classes(car_classes),
		transmission_type(transmission_type),
		city(city),
		is_rent(false)
	{
	}

	string to_string();

	static Car parse(string value);
private:
	Car(const string& id, const string& name, const std::byte& rating, float fuel_consumption, int pledge, int rental_prices, const BodyType& body_type, const CarClasses& car_classes, const TransmissionType& transmission_type, const City& city, bool is_rent)
		: id(id), name(name), rating(rating), fuel_consumption(fuel_consumption), pledge(pledge), rental_prices(rental_prices), body_type(body_type), car_classes(car_classes), transmission_type(transmission_type), city(city), is_rent(is_rent)
	{
	}
};

