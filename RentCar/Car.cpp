#include "Car.h"

string Car::get_id()
{
	return id;
}

void Car::set_name(string new_name)
{
	if (new_name.empty())
	{
		throw invalid_argument("������ ������.");
	}

	if (new_name.size() > MAX_LENGTH_NAME)
	{
		throw invalid_argument("������ ������ 10 ��������");
	}

	name = new_name;
}

string Car::get_name()
{
	return name;
}

void Car::set_rating(byte new_rating)
{
	int r = static_cast<int>(new_rating);
	if (r > -1 && r < 6)
	{
		rating = new_rating;
		return;
	}

	throw exception("������� ������ ���� �� 0 �� 5");
}

byte Car::get_rating()
{
	return rating;
}

void Car::set_fuel_consumption(float new_fuel_consumption)
{
	if (new_fuel_consumption < 0.01)
	{
		throw exception("�������� ������ ���� ������ 0.");
	}

	fuel_consumption = new_fuel_consumption;
}

float Car::get_fuel_consumption()
{
	return fuel_consumption;
}

void Car::set_pledge(int new_pladge)
{
	if (new_pladge > 0)
	{
		pledge = new_pladge;
		return;
	}

	throw exception("����� ������ ���� ������ 0.");
}

int Car::get_pledge()
{
	return pledge;
}

void Car::set_rental_prices(int new_rental_prices)
{
	if (new_rental_prices < 1)
	{
		throw exception("����� ���� ������ ���� ������ 0.");
	}

	rental_prices = new_rental_prices;
}

int Car::get_rental_prices()
{
	return rental_prices;
}

void Car::set_body_type(BodyType new_body_type)
{
	body_type = new_body_type;
}

BodyType Car::get_body_type()
{
	return body_type;
}

void Car::set_car_classes(CarClasses new_car_classes)
{
	car_classes = new_car_classes;
}

CarClasses Car::get_car_classes()
{
	return car_classes;
}

void Car::set_transmission_type(TransmissionType new_transmission_type)
{
	transmission_type = new_transmission_type;
}

TransmissionType Car::get_transmission_type()
{
	return transmission_type;
}

void Car::set_city(City new_city)
{
	city = new_city;
}

City Car::get_city()
{
	return city;
}

void Car::set_is_rent(bool new_is_rent)
{
	is_rent = new_is_rent;
}

bool Car::get_is_rent()
{
	return is_rent;
}

string Car::to_string()
{
	stringstream ss;

	ss << id << FILE_SEPARATOR 
		<< name << FILE_SEPARATOR
		<< static_cast<int>(rating) << FILE_SEPARATOR
		<< fuel_consumption << FILE_SEPARATOR
		<< pledge << FILE_SEPARATOR
		<< rental_prices << FILE_SEPARATOR
		<< body_type << FILE_SEPARATOR
		<< car_classes << FILE_SEPARATOR
		<< transmission_type << FILE_SEPARATOR
		<< city << FILE_SEPARATOR
		<< is_rent;

	return ss.str();
}

Car Car::parse(string value)
{
	size_t start = 0, end = 0;
	vector<string> vector;

	while((start = value.find_first_not_of(FILE_SEPARATOR, end)) != string::npos)
	{
		end = value.find(FILE_SEPARATOR, start);
		vector.push_back(value.substr(start, end - start));
	}

	return Car(vector[0],
				vector[1],
				std::byte(stoi(vector[2])),
				stof(vector[3]),
				stoi(vector[4]),
				stoi(vector[5]),
				(BodyType)stoi(vector[6]),
				(CarClasses)stoi(vector[7]),
				(TransmissionType)stoi(vector[8]),
				(City)stoi(vector[9]),
				(bool)stoi(vector[10]));
}
