#include "GetDateTime.h"

void GetDateTime::get_date_time_now(int add_day)
{
	// �������� ������� ���� � �����
	auto now = chrono::system_clock::now();

	if (add_day > 0)
	{
		now += chrono::hours(add_day * 24);
	}

	// ����������� ������� ���� � ����� � ��������� "std::tm"
	time_t now_c = chrono::system_clock::to_time_t(now);
	tm now_tm;

	if (localtime_s(&now_tm, &now_c) == 0)
	{
		// ��������� ���, ����� � ����
		year = now_tm.tm_year + 1900; // ��� ������������ � 1900
		month = now_tm.tm_mon + 1;    // ������ ���������� � 0 (������) �� 11 (�������)
		day = now_tm.tm_mday;         // ���� ������
	}
	else
	{
		//TODO: �������� ���������� ��� ������
	}
}

int GetDateTime::get_year() { return year; }
int GetDateTime::get_month() { return month; }
int GetDateTime::get_day() { return day; }