enum BodyType {
	Sedan,
	StationWagon
};

enum CarClasses {
	Steward,
	Average,
	Business,
	SUV,
	Premium,
};

enum TransmissionType {
	Mechanics,
	Machine,
};

enum City {
	Dnepr,
	Lviv,
	Kyiv,
	Odessa,
};