#include "Client.h"

string Client::get_id()
{
	return id;
}

void Client::set_name(string new_name)
{
	if (new_name.empty())
	{
		throw invalid_argument("������ ������.");
	}

	name = new_name;
}

string Client::get_name()
{
	return name;
}

void Client::set_second_name(string new_second_name)
{
	if (new_second_name.empty())
	{
		throw invalid_argument("������ ������.");
	}

	name = new_second_name;
}

string Client::get_second_name()
{
	return second_name;
}

void Client::set_number_passport(string new_number_passport)
{
	if (new_number_passport.empty())
	{
		throw invalid_argument("������ ������.");
	}

	regex pattern("^[�-�]{2}[0-9]{6}$");

	if (!regex_match(new_number_passport, pattern))
	{
		throw exception("������ �� ������ �� ����� ��������. ������ \"��123456\"");
	}

	number_passport = new_number_passport;
}

string Client::get_number_passport()
{
	return number_passport;
}

string Client::get_id_car()
{
	return id_car;
}

chrono::year_month_day Client::get_date()
{
	return date;
}

void Client::set_card_number(string new_card_number)
{
	if (new_card_number.empty())
	{
		throw invalid_argument("������ ������.");
	}

	regex pattern("^[0 - 9]{16}$");

	if (!regex_match(new_card_number, pattern))
	{
		throw exception("������ �� ������ �� ����� ���������� �����.");
	}

	name = new_card_number;
}

string Client::get_card_number()
{
	return card_number;
}

string Client::to_string()
{
	stringstream ss;

	ss << id << FILE_SEPARATOR
		<< name << FILE_SEPARATOR
		<< second_name << FILE_SEPARATOR
		<< number_passport << FILE_SEPARATOR
		<< id_car << FILE_SEPARATOR
		<< date << FILE_SEPARATOR
		<< card_number;

	return ss.str();
}

Client Client::parse(string s)
{
	size_t start = 0, end = 0;
	vector<string> vector;

	while ((start = s.find_first_not_of(FILE_SEPARATOR, end)) != string::npos)
	{
		end = s.find(FILE_SEPARATOR, start);
		vector.push_back(s.substr(start, end - start));
	}

	istringstream iss(vector[5]);
	chrono::year_month_day date;
	int year, month, day;
	char delimiter;

	if (iss >> year >> delimiter >> month >> delimiter >> day)
	{
		date = chrono::year_month_day(chrono::year(year), chrono::month(month), chrono::day(day));
	}

	return Client(vector[0],
		vector[1],
		vector[2],
		vector[3],
		vector[4],
		date,
		vector[6]);
}