#include "RandomString.h"

string RandomString::get_random_string(const size_t& length)
{
    // ��������� ��������� �����
    random_device rd;
    mt19937 gen(rd());
    size_t one = 1;

    string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    uniform_int_distribution<> distribution(0, chars.size() - one);

    string randomString;
    size_t i = 0;

    while (i < length) { 
        int index = distribution(gen);
        randomString += chars[index];
        ++i;
    }

    return randomString;
}
