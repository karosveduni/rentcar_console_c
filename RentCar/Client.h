#pragma once
#include <string>
#include <chrono>
#include <regex>

#include "ConstValue.cpp"
#include "GetDateTime.h"
#include "RandomString.h"

using namespace std;

class Client
{
private:
	string id;
	string name;
	string second_name;
	string number_passport;
	string id_car;
	chrono::year_month_day date;
	string card_number;

	Client(const string& id, const string& name, const string& second_name, const string& number_passport, const string& id_car, const chrono::year_month_day& date, const string& card_number)
		: id(id), name(name), second_name(second_name), number_passport(number_passport), id_car(id_car), date(date), card_number(card_number)
	{
	}

public:

	string get_id();

	void set_name(string new_name);
	string get_name();

	void set_second_name(string new_second_name);
	string get_second_name();

	void set_number_passport(string new_number_passport);
	string get_number_passport();

	string get_id_car();

	chrono::year_month_day get_date();

	void set_card_number(string new_card_number);
	string get_card_number();

	Client(const string& name, const string& second_name, const string& number_passport, const string& id_car, const string& card_number, const int& amount_of_days)
		: name(name), second_name(second_name), number_passport(number_passport), id_car(id_car), card_number(card_number)
	{
		id = RandomString::get_random_string(MAX_LENGTH_ID);
		GetDateTime::get_instance().get_date_time_now(amount_of_days);
		date = chrono::year_month_day(chrono::year(GetDateTime::get_instance().get_year()), chrono::month(GetDateTime::get_instance().get_month()), chrono::day(GetDateTime::get_instance().get_day()));
	}

	bool operator==(const Client& other) const
	{
		return name == other.name
			&& second_name == other.second_name
			&& number_passport == other.number_passport
			&& id_car == other.id_car
			&& date == other.date &&
			card_number == other.card_number;
	}

	bool operator!=(const Client& other) const {
		return !(*this == other);
	}

	Client() = default;

	string to_string();

	static Client parse(string s);
};

